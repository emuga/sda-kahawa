               <!-- ***** Donate section ***** -->
                <section class="donate-section" id="donateSection">
                    <div class="inner">
                        <div class="row">
                            <div class="medium-12 columns">
                                <h2><span>Want to Donate?</span> It’s time to show your huminity</h2>
                                <a href="#" class="button">Donate Now</a>
                            </div>
                        </div>
                    </div>
                </section>