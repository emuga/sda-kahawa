<?php
/**
 * Send Contact
 *
 * This script send contact form data to email
 *
 * Template Name: Hope - Church Responsive HTML5 Template
 * Template URL: http://church.pistaciatheme.com
 * Description: Flat church and congregation HTML5 template
 * File name: contactform.php
 * Author: Pistaciatheme
 * Support: http://themeforest.net/user/pistaciatheme
 * Version: 1.4
 */

/* Config */
$emailto = "pistaciatheme@gmail.com";
$subject = "Contact form - Hope - Church Responsive HTML5 Template";

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')
{


    $rules = array(
        'firstname' => 'required',
        'lastname' => 'required',
        'phone' => 'required',
        'email' => 'required|email');

    $data = array(
        'firstname' => $_POST['contactFirstname'],
        'lastname' => $_POST['contactLastname'],
        'name' => $_POST['contactFirstname'],
        'phone' => $_POST['contactPhone'],
        'email' => $_POST['contactEmail'],
        'message' => $_POST['contactMessage']);

    require_once('class/Validator.php');
    $validator = new Validator();

    if ($validator->validate($data, $rules))
    {
        $to = $emailto;

        $emailTemplate='First name:' . $data['firstname'] .'<br>Last name:' . $data['lastname'] . '<br>Phone:' . $data['phone'] . '<br>Email:' . $data['email'].'<br>Message:'. $data['message'];

        $message = $emailTemplate;
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'To: ' .$emailto . "\r\n";
        $headers .= 'From: <' . $data['email'] . '>' . "\r\n";
        mail($to, $subject, $message, $headers);
    }
}
