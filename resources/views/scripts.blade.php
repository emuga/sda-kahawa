
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/jquery.cookie.js"></script>
        <script src="assets/js/foundation.js"></script>
        <script src="assets/js/classie.js"></script>
        <script src="assets/js/owl.carousel.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/jquery.countdown.js"></script>
        <script src="assets/js/jquery.counterup.js"></script>
        <script src="assets/js/jquery.scrollTo.js"></script>
        <script src="assets/js/parallax.js"></script>
        <script src="assets/js/jquery.waypoints.js"></script>
        <script src="assets/js/jquery.flexslider.js"></script>
        <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
        <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>

        <!-- Application main -->
        <script src="assets/js/application.js"></script>

        <script>
            APP.init();
            APP.initPlugins();
        </script>
