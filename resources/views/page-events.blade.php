<!DOCTYPE html>
<html lang="en">
@include('head')
    <body id="pageTop" class="fixed-navbar">
@include('theme-switcher')
@include('page-loader')
        <!-- ***** Canvas ***** -->
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
@include('left-off-canvas')
@include('right-off-canvas')
@include('header')
@include('revolution-slider')
                <!-- ***** Events section ***** -->
                <div class="events-section" id="eventsSection">
                    <div class="inner">
                        <div class="row item well">
                            <div class="medium-12 columns">
                                <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf; padding-top: 75px;">
                                     <h4>
                                         Children’s Ministries
                                     </h4>
                                     <p>
                                         Children’s ministries develops the faith of children from birth through age 14, leading them into union with the church. It seeks to provide multiple ministries that will lead children to jesus and disciple them in their daily walk with him. It cooperates with the sabbath school and other departments to provide religious education to children and fulfills its mission by developing a variety of grace-oriented ministries for children that are inclusive,service-oriented, leadership-building, safe, and evangelistic.
                                     </p>
                                    </div>
                                        <hr><hr>
                                </div>
                                <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf;padding-top: 10px;">
                                     <h4>
                                         Communication
                                     </h4>
                                     <p>
                                         Communication ministry calls for the support of every layperson, Church employee, and Church institution. The communication department promotes the use of a sound program of public relations and all contemporary communication techniques, sustainable technologies, and media in the promulgation of the gospel.
                                     </p>
                                    </div>
                                        <hr><hr>
                                </div>
                                <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf;">
                                     <h4>
                                        Family Ministries 
                                     </h4>
                                     <p>
                                         The objective of family ministries is to strengthen marriage and the family. The family was established by divine creation with marriage at its centre. Family ministries upholds the biblical teaching related to the family and lifts God’s ideals for family living. At the same time, it brings an understanding of the brokenness experienced by individuals and families in a fallen world.
                                     </p> 
                                    </div>
                                        <hr><hr>
                                </div>
                                <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf; border:1px, black">
                                     <h4>
                                        Health Ministries
                                     </h4>
                                     <p>
                                         The Church believes its responsibility to make Christ known to the world includes a moral obligation to preserve human dignity by promoting optimal levels of physical, mental, and spiritual health. In addition to ministering to those who are ill, this responsibility extends to the prevention of disease through effective health education and leadership in promoting optimum health, free of tobacco, alcohol, other drugs, and unclean foods. Where possible, members shall be encouraged to follow a primarily vegetarian diet.
                                     </p>
                                    </div>
                                        <hr><hr>
                                </div>
                                    <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf; border:1px, black">
                                     <h4>
                                       Public Affairs and Religious Liberty 
                                     </h4>
                                     <p>
                                         The Church believes its responsibility to make Christ known to the world includes a moral obligation to preserve human dignity by promoting optimal levels of physical, mental, and spiritual health. In addition to ministering to those who are ill, this responsibility extends to the prevention of disease through effective health education and leadership in promoting optimum health, free of tobacco, alcohol, other drugs, and unclean foods. Where possible, members shall be encouraged to follow a primarily vegetarian diet.

                                     </p>
                                    </div>
                                        <hr><hr>
                                </div>
                                    <div class="row">
                                <div class="medium-12 columns well" style=" background: #bfbfbf; border:1px, black">
                                     <h4>
                                       Sabbath School 
                                     </h4>
                                     <p>
                                         The Church believes its responsibility to make Christ known to the world includes a moral obligation to preserve human dignity by promoting optimal levels of physical, mental, and spiritual health.In addition to ministering to those who are ill, this responsibility extends to the prevention of disease through effective health education and leadership in promoting optimum health, free of tobacco, alcohol, other drugs, and unclean foods. Where possible, members shall be encouraged to follow a primarily vegetarian diet.
                                    </p>
                                    </div>
                                        <hr><hr>
                                </div>
                                    <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf; border:1px, black">
                                     <h4>
                                       Personal Ministries
                                     </h4>
                                     <p>
                                         Personal ministries provide resources and trains members to unite their efforts with those of the pastor and officers in soul-winning service. It also has primary responsibility for programs assisting those in need.

                                     </p>
                                    </div>
                                        <hr><hr>
                                </div>
                                    <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf; border:1px, black">
                                     <h4>
                                       Stewardship Ministries
                                     </h4>
                                     <p>
                                         Stewardship ministries encourages members to respond to God’s grace by dedicating all they have to Him. Stewardship responsibility involves more than just money. It includes, but is not limited to, the proper care and use of the body, mind, time, abilities, spiritual gifts, relationships, influence, language, the environment, and material possessions. The department assists members in their partnership with God in completing His mission through the proper utilization of all of His gifts and resources.
                                     </p>
                                    </div>
                                        <hr><hr>
                                </div>
                                 <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf; border:1px, black">
                                     <h4>
                                       Women's Ministries
                                     </h4>
                                     <p>
                                         Women’s ministries upholds, encourages, and challenges women in their daily walk as disciples of Jesus Christ and as members of His church. Its objectives are to foster spiritual growth and renewal; affirm that women are of immeasurable worth by virtue of their creation and redemption, equip them for service, and offer women’s perspectives on church issues; minister to the broad spectrum of women’s needs, with regard for multicultural and multiethnic perspectives; cooperate with other departments to facilitate ministry to women and of women; build good will among women to encourage mutual support and creative exchange of ideas; mentor and encourage women and create paths for their involvement in the church; and find ways and means to challenge each woman to use her gifts to further global mission.

                                     </p>
                                    </div>
                                        <hr><hr>
                                </div>

                                <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf; border:1px, black">
                                     <h4>
                                       Ambassador Club
                                     </h4>
                                     <p>
                                        The Ambassador Club provides a specialized program to meet the needs of youth, ages 16 through 21. It offers young people in this age group organization and structure, and promotes their active involvement in the church, locally and globally. The club is designed to strengthen the current senior youth/young adult ministry of the Church. It challenges them to experience and share a personal relationship with Christ, helps them develop a lifestyle that fits their belief system and vocational interest, and provides them with a safe venue for wholesome development of lifelong friendships.


                                     </p>
                                    </div>
                                        <hr><hr>
                                </div>

                                    <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf; border:1px, black">
                                     <h4>
                                      Pathfinder Club
                                     </h4>
                                     <p>
                                        The Pathfinder Club provides a church-centered outlet for the spirit of adventure and exploration found in junior youth. This includes carefully tailored activities in outdoor living, nature exploration, crafts, hobbies, or vocations beyond the possibilities in an average AJY. In this setting spiritual emphasis is well received, and the Pathfinder Club has demonstrated its soul-winning influence.
                                     </p>
                                    </div>
                                        <hr><hr>
                                </div>

                                <div class="row">
                                    <div class="medium-12 columns well" style=" background: #bfbfbf; border:1px, black">
                                     <h4>
                                      Adventurer Club
                                     </h4>
                                     <p>
                                        The Adventurer Club provides home and church programs for parents with 6- through 9-year-old children. It is designed to stimulate the children’s curiosity and includes age-specific activities that involve both parent and child in recreational activities, simple crafts, appreciation of God’s creation, and other activities that are of interest to that age. All is carried out with a spiritual focus, setting the stage for participation in the church as a Pathfinder.
                                     </p>
                                    </div>
                                        <hr><hr>
                                </div>
                                <div class="row item">
                                    <div class="columns medium-6 photo">
                                        <img src="assets/img/content/placeholder-low.gif" alt="">
                                    </div>
                                    <div class="columns medium-6 info">
                                        <h5><a class="font-lora" href="#.">Donate For Handicapped Child </a></h5>
                                        <div class="date-place">
                                            <span><i class="fa fa-clock-o"></i> May 14, 2016 2.00 pm</span>
                                                <span><i class="fa fa-user"></i> Phasellus ullamcorper</span>
                                        </div>
                                        <p>Etiam purus libero, molestie eu placerat id, laoreet eget libero. Phasellus ullamcorper ligula ipsum, non</p>
                                        <a class="button btn-white" href="#">Join now</a>
                                    </div><!-- /.info -->
                                </div><!-- /.item -->
                                <div class="row item">
                                    <div class="columns medium-6 photo">
                                        <img src="assets/img/content/placeholder-low.gif" alt="">
                                    </div>
                                    <div class="columns medium-6 info">
                                        <h5><a class="font-lora" href="#.">Donate For Handicapped Child </a></h5>
                                        <div class="date-place">
                                            <span><i class="fa fa-clock-o"></i> May 14, 2016 2.00 pm</span>
                                            <span><i class="fa fa-user"></i> Phasellus ullamcorper</span>
                                        </div>
                                        <p>Etiam purus libero, molestie eu placerat id, laoreet eget libero. Phasellus ullamcorper ligula ipsum, non</p>
                                        <a class="button btn-white" href="#">Join now</a>
                                    </div><!-- /.info -->
                                </div><!-- /.item -->
                                <div class="row item">
                                    <div class="columns medium-6 photo">
                                        <img src="assets/img/content/placeholder-low.gif" alt="">
                                    </div>
                                    <div class="columns medium-6 info">
                                        <h5><a class="font-lora" href="#.">Donate For Handicapped Child </a></h5>
                                        <div class="date-place">
                                            <span><i class="fa fa-clock-o"></i> May 14, 2016 2.00 pm</span>
                                            <span><i class="fa fa-user"></i> Phasellus ullamcorper</span>
                                        </div>
                                        <p>Etiam purus libero, molestie eu placerat id, laoreet eget libero. Phasellus ullamcorper ligula ipsum, non</p>
                                        <a class="button btn-white" href="#">Join now</a>
                                    </div><!-- /.info -->
                                </div><!-- /.item -->
                                <div class="row item">
                                    <div class="columns medium-6 photo">
                                        <img src="assets/img/content/placeholder-low.gif" alt="">
                                    </div>
                                    <div class="columns medium-6 info">
                                        <h5><a class="font-lora" href="#.">Donate For Handicapped Child </a></h5>
                                        <div class="date-place">
                                            <span><i class="fa fa-clock-o"></i> May 14, 2016 2.00 pm</span>
                                            <span><i class="fa fa-user"></i> Phasellus ullamcorper</span>
                                        </div>
                                        <p>Etiam purus libero, molestie eu placerat id, laoreet eget libero. Phasellus ullamcorper ligula ipsum, non</p>
                                        <a class="button btn-white" href="#">Join now</a>
                                    </div><!-- /.info -->
                                </div><!-- /.item -->
                                <div class="pagination-centered">
                                    <ul class="pagination" role="menubar" aria-label="Pagination">
                                        <li class="arrow unavailable" aria-disabled="true"><a href="">&laquo; Previous</a></li>
                                        <li class="current"><a href="">1</a></li>
                                        <li><a href="">2</a></li>
                                        <li><a href="">3</a></li>
                                        <li><a href="">4</a></li>
                                        <li class="unavailable" aria-disabled="true"><a href="">&hellip;</a></li>
                                        <li><a href="">12</a></li>
                                        <li><a href="">13</a></li>
                                        <li class="arrow"><a href="">Next &raquo;</a></li>
                                    </ul>
                                </div>
                            </div><!-- /.columns -->
                            <div class="medium-3 columns">
                                <div class="blog-box">
                                    <div class="inner">
                                        <div class="ws-title left-line">
                                            <h1 class="title">Categories</h1>
                                        </div>
                                        <div class="content">
                                            <ul class="list-arrow categories">
                                                <li>
                                                    <a href="#">
                                                        irem ipsum dolor sit
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        amet consetetur
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        elitr sed diam nonumy
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        eirmod tempor invidunt
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        ut labore et dolore
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        orem ipsum dolor sit
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        amet consetetur
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        irem ipsum dolor sit
                                                    </a>
                                                </li>
                                            </ul>
                                        </div><!-- /.content -->
                                    </div><!-- /.inner -->
                                </div><!-- /.blog-box -->
                                <div class="blog-box">
                                    <div class="inner">
                                        <div class="ws-title left-line">
                                            <h1 class="title">Archives</h1>
                                        </div>
                                        <div class="content">
                                            <ul class="list-arrow">
                                                <li>
                                                    <a href="#">
                                                        2016 January (12)
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        2016 February (21)
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        2016 March (89)
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        2016 April (102)
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        2016 May (15)
                                                    </a>
                                                </li>
                                            </ul>
                                        </div><!-- /.content -->
                                    </div><!-- /.inner -->
                                </div><!-- /.blog-box -->
                            </div><!-- /.columns -->
                        </div><!-- /.row -->
                    </div><!-- /.inner -->
                </div><!-- /.events-section -->



                <!-- ***** Footer section ***** -->
                <footer class="footer-section" id="footerSection">
                    <div class="content">
                        <div class="inner">
                            <div class="row">
                                <div class="large-3 columns">
                                    <img alt="" src="assets/img/elements/cross.png">
                                </div><!-- /.columns -->
                                <div class="large-3 columns info-box">

                                    <h1>About Pistacia Church</h1>
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p>

                                    <h1>Our church on Twitter</h1>
                                    <ul class="twitter-roll">
                                        <li><i class="fa fa-twitter-square"></i> <a href="#">@pistaciachurch</a> stet clita kasd gubergren, no sea takimata July. 18, 2015 nulla convallis egestas rhmauris quam</li>
                                        <li><i class="fa fa-twitter-square"></i> <a href="#">@pistaciachurch</a> consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam July. 18, 2015 Nulla convallis egestas rhmauris quam</li>
                                    </ul>

                                </div><!-- /.columns -->
                                <div class="large-3 columns blogroll-box">
                                    <h1>Blogroll</h1>
                                    <ul class="list-arrow">
                                        <li><a href="#">irem ipsum dolor sit</a></li>
                                        <li><a href="#">amet consetetur</a></li>
                                        <li><a href="#">elitr sed diam nonumy</a></li>
                                        <li><a href="#">eirmod tempor invidunt</a></li>
                                        <li><a href="#">ut labore et dolore</a></li>
                                        <li><a href="#">orem ipsum dolor sit</a></li>
                                        <li><a href="#">amet consetetur</a></li>
                                        <li><a href="#">irem ipsum dolor sit</a></li>
                                    </ul>
                                </div><!-- /.columns -->
                                <div class="large-3 columns contact-box">
                                    <img alt="" src="assets/img/logo/logo-white.png">
                                    <ul class="contact">
                                        <li>753 A 4th St, NY, USA</li>
                                        <li>+5 432-234-1234</li>
                                        <li>me@medomain.com</li>
                                    </ul>
                                    <div class="text-center">
                                        <a href="#" class="button btn-dark">Contact us</a>
                                    </div>
                                    <ul class="socials">
                                        <li><a href="#" class="icon"><i class="fa fa-facebook-official"></i></a></li>
                                        <li><a href="#" class="icon"><i class="fa fa-twitter-square"></i></a></li>
                                        <li><a href="#" class="icon"><i class="fa fa-youtube-square"></i></a></li>
                                        <li><a href="#" class="icon"><i class="fa fa-pinterest-square"></i></a></li>
                                        <li><a href="#" class="icon"><i class="fa fa-skype"></i></a></li>
                                    </ul>
                                </div><!-- /.columns -->
                            </div><!-- /.row -->
                        </div><!-- /.inner -->
                    </div><!-- /.content -->
                    <div class="copyright">
                        <div class="inner">
                            <div class="row">
                                <div class="medium-10 columns">
                                    Hope - Church Responsive HTML5 Template by <a href="http://themeforest.net/item/pistacia-chef-food-html5-template/10800108">Pistaciatheme</a>
                                </div>
                                <div class="medium-2 columns">
                                    <a href="pageTop" class="scroll-to"><i class="fa fa-angle-up"></i>Page top</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fixed-wrapper">
                        <div class="row">
                            <div class="medium-2 large-2 large-push-4 columns">
                                <img alt="" src="assets/img/content/placeholder-thumb.gif">
                            </div>
                            <div class="medium-10 large-4 large-pull-2 columns">
                                <div class="thanx-box">
                                    <h1>Thank You</h1>
                                    <h2>for visiting</h2>
                                    <p><span class="strong">Contact me</span> accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer><!-- #footerSection -->



                <!-- ***** Search modals ***** -->
                <div class="ps-modal-overlay" id="searchModal">
                    <a class="overlay-close"></a>
                    <div class="search row">
                        <div class="large-12 columns">
                            <form>
                                <div class="row collapse">
                                    <div class="small-8 columns">
                                        <input type="text" placeholder="Search...">
                                    </div>
                                    <div class="small-4 columns">
                                        <a href="#" class="button"><i class="fa fa-search"></i></a>
                                    </div>
                                </div><!-- /.row -->
                            </form>
                        </div><!-- /.large-12 -->
                    </div><!-- /.search -->
                    <div class="recommends row">
                        <div class="large-12 columns">
                            <div class="row">
                                <div class="large-4 columns">
                                    <h4>People</h4>
                                    <ul class="items">
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-circle">Guy Wilkins</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-circle">Larry Reed</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-circle">Roxanne Holland</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-circle">Bridget Kennedy</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-circle">Sally Mendoza</a></li>
                                    </ul><!-- /.items -->
                                </div><!-- /.large-4 -->
                                <div class="large-4 columns">
                                    <h4>Sermons</h4>
                                    <ul class="items">
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Guy Wilkins</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Larry Reed</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Roxanne Holland</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Bridget Kennedy</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Sally Mendoza</a></li>
                                    </ul><!-- /.items -->
                                </div><!-- /.large-4 -->
                                <div class="large-4 columns">
                                    <h4>Recent</h4>
                                    <ul class="items">
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumbe">Guy Wilkins</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Larry Reed</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Roxanne Holland</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Bridget Kennedy</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Sally Mendoza</a></li>
                                    </ul><!-- /.items -->
                                </div><!-- /.large-4 -->
                            </div><!-- /.row -->
                        </div><!-- /.large-12 -->
                    </div><!-- /.recommends -->
                </div><!-- /#searchModal -->

                <a class="exit-off-canvas"></a>

            </div>
        </div>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/jquery.cookie.js"></script>
        <script src="assets/js/foundation.js"></script>
        <script src="assets/js/classie.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/jquery.countdown.js"></script>
        <script src="assets/js/jquery.scrollTo.js"></script>
        <script src="assets/js/jquery.waypoints.js"></script>

        <!-- Application main -->
        <script src="assets/js/application.js"></script>

        <script>
            APP.init();
            APP.initPlugins();
        </script>

    </body>
</html>