<!DOCTYPE html>
@include('head')
    <body id="pageTop" class="fixed-navbar">

@include('theme-switcher')
@include('page-loader')
       



        <!-- ***** Canvas ***** -->
        <div class="off-canvas-wrap" data-offcanvas>

            <div class="inner-wrap">

              
@include('left-off-canvas')
@include('right-off-canvas')
@include('header')
@include('revolution-slider')
@include('donate')
@include('service-box')

                <!-- ***** Testimonial section ***** -->
                <section class="testimonial-section" id="pastorQuotes">
                    <div class="inner">
                        <div class="row">
                            <div class="flexslider flexslider-testimonial">
                                <ul class="slides">
                                    <li class="item">
                                        <div class="table">
                                            <div class="text-center title">
                                                <h1>"My heart rejoices in the Lord in <span>the Lord my horn</span> is lifted high."</h1>
                                                <span>Joseph Walker / singer</span>
                                            </div>
                                        </div><!-- .table -->
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                    </li><!-- .item -->
                                    <li class="item">
                                        <div class="table">
                                            <div class="text-center title">
                                                <h1>"By the time Lot reached Zoar, <span>the sun had</span> risen over the land"</h1>
                                                <span>John Lorem / pastor</span>
                                            </div>
                                        </div><!-- .table -->
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                    </li><!-- .item -->
                                    <li class="item">
                                        <div class="table">
                                            <div class="text-center title">
                                                <h1>"There has not risen anyone greater than <span>John the Baptist</span>"</h1>
                                                <span>Johny Walker / pastor</span>
                                            </div>
                                        </div><!-- .table -->
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                    </li><!-- .item -->
                                </ul><!-- .slides -->
                            </div><!-- .flexslider-testimonial -->
                        </div><!-- .row -->
                    </div><!-- .inner -->
                </section><!-- .testimonial-section -->



                <!-- ***** Testimony section ***** -->
                <section class="testimony-section" id="testimonySection">
                    <div class="inner animated" data-animation="fadeIn" data-animation-delay="0">
                        <div class="row">
                            <div class="carousel-wrapper">
                                <div class="owl-carousel testimony-carousel" id="testimonyCarousel">
                                    <div class="item quote-wrapper">
                                        <blockquote class="quote-content">
                                            <div class="quote-text">For I know the plans I have for you, declares the LORD, plans for welfare and not for evil, to give you a future and a hope...</div>
                                            <span class="author">Jeremiah 29:11</span>
                                            <span class="quote-decor"><i class="fa fa-quote-right"></i></span>
                                        </blockquote>
                                        <div class="clearfix">
                                            <div class="photo">
                                                <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                            </div>
                                            <div class="people">
                                                <div class="name">Medhros Amarmiel</div>
                                                <div class="years">23 years</div>
                                                <a href="#" class="button">more</a>
                                            </div>
                                        </div>
                                    </div><!-- .item -->
                                    <div class="item quote-wrapper">
                                        <blockquote class="quote-content">
                                            <div class="quote-text">For I know the plans I have for you, declares the LORD, plans for welfare and not for evil, to give you a future and a hope...</div>
                                            <span class="author">Jeremiah 29:11</span>
                                            <span class="quote-decor"><i class="fa fa-quote-right"></i></span>
                                        </blockquote>
                                        <div class="clearfix">
                                            <div class="photo">
                                                <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                            </div>
                                            <div class="people">
                                                <div class="name">Medhros Amarmiel</div>
                                                <div class="years">23 years</div>
                                                <a href="#" class="button">more</a>
                                            </div>
                                        </div>
                                    </div><!-- .item -->
                                    <div class="item quote-wrapper">
                                        <blockquote class="quote-content">
                                            <div class="quote-text">For I know the plans I have for you, declares the LORD, plans for welfare and not for evil, to give you a future and a hope...</div>
                                            <span class="author">Jeremiah 29:11</span>
                                            <span class="quote-decor"><i class="fa fa-quote-right"></i></span>
                                        </blockquote>
                                        <div class="clearfix">
                                            <div class="photo">
                                                <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                            </div>
                                            <div class="people">
                                                <div class="name">Medhros Amarmiel</div>
                                                <div class="years">23 years</div>
                                                <a href="#" class="button">more</a>
                                            </div>
                                        </div>
                                    </div><!-- .item -->
                                    <div class="item quote-wrapper">
                                        <blockquote class="quote-content">
                                            <div class="quote-text">For I know the plans I have for you, declares the LORD, plans for welfare and not for evil, to give you a future and a hope...</div>
                                            <span class="author">Jeremiah 29:11</span>
                                            <span class="quote-decor"><i class="fa fa-quote-right"></i></span>
                                        </blockquote>
                                        <div class="clearfix">
                                            <div class="photo">
                                                <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                            </div>
                                            <div class="people">
                                                <div class="name">Medhros Amarmiel</div>
                                                <div class="years">23 years</div>
                                                <a href="#" class="button">more</a>
                                            </div>
                                        </div>
                                    </div><!-- .item -->
                                </div><!-- .owl-carousel -->
                            </div><!-- .carousel-wrapper -->
                        </div><!-- .row -->
                    </div><!-- .inner -->
                </section><!-- .testimony-section -->



                <!-- ***** Dark line section ***** -->
                <section class="dark-line-section with-triangle" id="contactFormSection">
                    <div class="inner">
                        <div class="row">
                            <div class="medium-6 columns">
                                <div class="ws-title text-center">
                                    <h1>Connect worship group</h1>
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore</p>
                                </div>
                                <form class="dark" id="contactForm">
                                    <input type="hidden" name="contactSec" id="contactSec"/>
                                    <div class="row">
                                        <div class="medium-6 columns">
                                            <div class="row collapse">
                                                <div class="small-10 columns">
                                                    <input type="text" name="contactFirstname" id="contactFirstname" placeholder="First Name">
                                                </div>
                                                <div class="small-2 columns">
                                                    <span class="postfix"><i class="fa fa-user"></i></span>
                                                </div>
                                            </div>
                                        </div><!-- .columns -->
                                        <div class="medium-6 columns">
                                            <div class="row collapse">
                                                <div class="small-10 columns">
                                                    <input type="text" name="contactLastname" id="contactLastname" placeholder="Last name">
                                                </div>
                                                <div class="small-2 columns">
                                                    <span class="postfix"><i class="fa fa-user"></i></span>
                                                </div>
                                            </div>
                                        </div><!-- .columns -->
                                    </div><!-- .row -->
                                    <div class="row">
                                        <div class="medium-6 columns">
                                            <div class="row collapse">
                                                <div class="small-10 columns">
                                                    <input type="text" name="contactEmail" id="contactEmail" placeholder="E-mail">
                                                </div>
                                                <div class="small-2 columns">
                                                    <span class="postfix"><i class="fa fa-envelope"></i></span>
                                                </div>
                                            </div>
                                        </div><!-- .columns -->
                                        <div class="medium-6 columns">
                                            <div class="row collapse">
                                                <div class="small-10 columns">
                                                    <input type="text" placeholder="Phone"  name="contactPhone" id="contactPhone">
                                                </div>
                                                <div class="small-2 columns">
                                                    <span class="postfix"><i class="fa fa-mobile"></i></span>
                                                </div>
                                            </div>
                                        </div><!-- .columns -->
                                    </div><!-- .row -->
                                    <div class="row">
                                        <div class="medium-12 columns">
                                            <div class="row collapse postfix-radius">
                                                <div class="small-12 columns">
                                                    <textarea placeholder="Message..." name="contactMessage" id="contactMessage"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .row -->
                                    <div class="row">
                                        <div class="medium-12 columns">
                                            <span class="success-message">Seccess check in</span>
                                            <span class="alert-message">Alert check in</span>
                                        </div>
                                    </div><!-- .row -->
                                    <div class="row">
                                        <div class="medium-12 columns text-left">
                                            <a href="#" class="button btn-send"><i class="fa fa-paper-plane"></i> Send</a>
                                        </div>
                                    </div><!-- .row -->
                                </form>
                            </div>
                            <div class="medium-6 columns photo">
                                <img alt="" src="assets/img/elements/young-woman.png">
                            </div>
                        </div><!-- .row -->
                    </div>
                </section>



                <!-- ***** Galleries section ***** -->
                <section class="galleries-section" id="galleriesSection">
                    <div class="inner animated" data-animation="fadeIn" data-animation-delay="200">
                        <div class="row">
                            <div class="medium-6 medium-offset-3 columns">
                                <div class="ws-title text-center">
                                    <h1>Celebrate galleries</h1>
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore</p>
                                </div>
                            </div>
                        </div><!-- .row -->
                        <div class="row text-center">
                            <ul class="list-tabs">
                                <li class="active"><a href="#">All</a></li>
                                <li><a href="#">2013</a></li>
                                <li><a href="#">2014</a></li>
                                <li><a href="#">2015</a></li>
                                <li><a href="#">2016</a></li>
                            </ul>
                        </div><!-- .row -->
                        <div class="carousel-wrapper">
                            <div class="owl-carousel galeries-carousel" id="galeriesCarousel">
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>1 jul, 2015</h2>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>2 jul, 2015</h2>
                                            <p>02Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>3 jul, 2015</h2>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>4 jul, 2015</h2>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>5 jul, 2015</h2>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>6 jul, 2015</h2>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>7 jul, 2015</h2>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>8 jul, 2015</h2>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>9 jul, 2015</h2>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>10 jul, 2015</h2>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                                <div class="item">
                                    <figure class="effect-jumbo">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <figcaption>
                                            <h2>11 jul, 2015</h2>
                                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                                            <a href="#" class="button btn-link">more</a>
                                            <ul class="icons">
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                                <li><a href="#"><i class="fa fa-paperclip"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                            </ul>
                                        </figcaption>
                                    </figure>
                                </div><!-- .item -->
                            </div><!-- .inner -->
                        </div><!-- .carousel-wrapper -->
                    </div><!-- .inner -->
                </section><!-- #galleriesSection -->



                <!-- ***** Dark line section ***** -->
                <section class="dark-line-section with-triangle">
                    <div class="inner animated" data-animation="fadeIn" data-animation-delay="0">
                        <div class="row">
                            <div class="small-12 columns">
                                <div class="ws-title text-center">
                                    <h1>Archivement</h1>
                                </div><!-- .ws-title -->
                                <div class="row">
                                    <div class="medium-6 medium-offset-3 columns">
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore</p>
                                    </div>
                                </div><!-- .row -->
                                <div class="row blocks counter-numbers">
                                    <div class="medium-3 columns">
                                        <i class="flaticon-saxophone6"></i>
                                        <h2 class="counter">1,234,567.00</h2>
                                        <p>HTML5 church template for religions</p>
                                    </div>
                                    <div class="medium-3 columns">
                                        <i class="flaticon-instrument7"></i>
                                        <h2 class="counter">1,234,567.00</h2>
                                        <p>Church.pistaciatheme.com Church and religion template</p>
                                    </div>
                                    <div class="medium-3 columns">
                                        <i class="flaticon-heart258"></i>
                                        <h2 class="counter">1,234,567.00</h2>
                                        <p>Lorem ipsum dolor sit amet, Church template sadipscing elitr</p>
                                    </div>
                                    <div class="medium-3 columns">
                                        <i class="flaticon-familiar17"></i>
                                        <h2 class="counter">1,234,567.00</h2>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr</p>
                                    </div>
                                </div><!-- .blocks -->
                            </div><!-- .columns -->
                        </div><!-- .row -->
                    </div><!-- .inner -->
                </section><!-- .dark-line-section -->



                <!-- ***** Latests section ***** -->
                <section class="latests-section">
                    <div class="inner">
                        <div class="row">
                            <div class="medium-5 columns latest-news">
                                <div class="ws-title left-line">
                                    <h1 class="title">Latest news <a href="#" class="button">All posts</a></h1>
                                </div>
                                <div class="news-items row">
                                    <div class="item clearfix">
                                        <div class="medium-5 columns">
                                            <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        </div>
                                        <div class="medium-7 columns">
                                            <h1 class="title">Pistacia church post title</h1>
                                            <span class="date"><i class="fa fa-calendar"></i> on July 23, 2015</span>
                                            <ul class="small-block-grid-2">
                                                <li>
                                                    <ul class="socials">
                                                        <li><a href="#" class="icon"><i class="fa fa-facebook-official"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-twitter-square"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-youtube-square"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-pinterest-square"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-skype"></i></a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul class="stars">
                                                        <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                        <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                        <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <p>Donec consequat aliquam tincidunt. Aliquam pretium lacus dolor. Phasellus ipsum nunc, pretium vel eleifend vitae, tincidunt et turpis. Nam tempus turpis vitae erat molestie, ac semper dolor bibendum ....</p>
                                            <a href="#" class="button">More</a>
                                        </div>
                                    </div><!-- /.item -->
                                    <div class="item clearfix">
                                        <div class="medium-5 columns">
                                            <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        </div>
                                        <div class="medium-7 columns">
                                            <h1 class="title">Pistacia church post title</h1>
                                            <span class="date"><i class="fa fa-calendar"></i> on July 23, 2015</span>
                                            <ul class="small-block-grid-2">
                                                <li>
                                                    <ul class="socials">
                                                        <li><a href="#" class="icon"><i class="fa fa-facebook-official"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-twitter-square"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-youtube-square"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-pinterest-square"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-skype"></i></a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <ul class="stars">
                                                        <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                        <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                        <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <p>Donec consequat aliquam tincidunt. Aliquam pretium lacus dolor. Phasellus ipsum nunc, pretium vel eleifend vitae, tincidunt et turpis. Nam tempus turpis vitae erat molestie, ac semper dolor bibendum ....</p>
                                            <a href="#" class="button">More</a>
                                        </div>
                                    </div><!-- /.item -->
                                </div><!-- /.news-items -->
                            </div><!-- /.latest-news -->
                            <div class="medium-4 columns latest-sermons">
                                <div class="ws-title left-line">
                                    <h1 class="title">Latest sermons <a href="#" class="button">All posts</a></h1>
                                </div>
                                <div class="sermons-items">
                                    <div class="item clearfix">
                                        <div class="small-2 columns">
                                            <span class="day">12</span>
                                            <span class="month">July</span>
                                        </div>
                                        <div class="small-7 columns">
                                            <h1>There was a man sent from God</h1>
                                            <span class="date">Sunday | 10:00 am</span>
                                            <a href="#" class="button btn-white">more details</a>
                                        </div>
                                        <div class="small-3 columns">
                                            <ul class="icons">
                                                <li><a href="#"><i class="flaticon-speaker100"></i></a></li>
                                                <li><a href="#"><i class="flaticon-save"></i></a></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.item -->
                                    <div class="item clearfix">
                                        <div class="small-2 columns">
                                            <span class="day">12</span>
                                            <span class="month">July</span>
                                        </div>
                                        <div class="small-7 columns">
                                            <h1>There was a man sent from God</h1>
                                            <span class="date">Sunday | 10:00 am</span>
                                            <a href="#" class="button btn-white">more details</a>
                                        </div>
                                        <div class="small-3 columns">
                                            <ul class="icons">
                                                <li><a href="#"><i class="flaticon-speaker100"></i></a></li>
                                                <li><a href="#"><i class="flaticon-save"></i></a></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.item -->
                                    <div class="item clearfix">
                                        <div class="small-2 columns">
                                            <span class="day">12</span>
                                            <span class="month">July</span>
                                        </div>
                                        <div class="small-7 columns">
                                            <h1>There was a man sent from God</h1>
                                            <span class="date">Sunday | 10:00 am</span>
                                            <a href="#" class="button btn-white">more details</a>
                                        </div>
                                        <div class="small-3 columns">
                                            <ul class="icons">
                                                <li><a href="#"><i class="flaticon-speaker100"></i></a></li>
                                                <li><a href="#"><i class="flaticon-save"></i></a></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.item -->
                                    <div class="item clearfix">
                                        <div class="small-2 columns">
                                            <span class="day">12</span>
                                            <span class="month">July</span>
                                        </div>
                                        <div class="small-7 columns">
                                            <h1>There was a man sent from God</h1>
                                            <span class="date">Sunday | 10:00 am</span>
                                            <a href="#" class="button btn-white">more details</a>
                                        </div>
                                        <div class="small-3 columns">
                                            <ul class="icons">
                                                <li><a href="#"><i class="flaticon-speaker100"></i></a></li>
                                                <li><a href="#"><i class="flaticon-save"></i></a></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.item -->
                                </div><!-- /.sermons-items -->
                            </div><!-- /.latest-sermons -->
                            <div class="medium-3 columns most-commented">
                                <div class="ws-title left-line">
                                    <h1 class="title">Most commented</h1>
                                </div>
                                <div class="most-commneted-items">
                                    <div class="item clearfix">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                        <h1 class="title">Pistacia church post title</h1>
                                        <span class="date"><i class="fa fa-calendar"></i> on July 23, 2015</span>
                                        <ul class="small-block-grid-2">
                                            <li>
                                                <ul class="socials">
                                                    <li><a href="#" class="icon"><i class="fa fa-facebook-official"></i></a></li>
                                                    <li><a href="#" class="icon"><i class="fa fa-twitter-square"></i></a></li>
                                                    <li><a href="#" class="icon"><i class="fa fa-youtube-square"></i></a></li>
                                                    <li><a href="#" class="icon"><i class="fa fa-pinterest-square"></i></a></li>
                                                    <li><a href="#" class="icon"><i class="fa fa-skype"></i></a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <ul class="stars">
                                                    <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                    <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                    <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                    <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhmauris quam....</p>
                                        <a href="#" class="button">More</a>
                                    </div><!-- /.item -->
                                </div><!-- /.most-commented-items -->
                            </div><!-- /.most-commented -->
                        </div><!-- /.row -->
                    </div><!-- /.inner -->
                </section><!-- /.latest-section -->



                <!-- ***** Parallax section ***** -->
                <section class="parallax-section" id="subscribeNewsletter">
                    <div class="inner animated" data-animation="fadeIn" data-animation-delay="0">
                        <div class="row">
                            <div class="medium-8 medium-offset-2 columns text-center">
                                <h1>Newsletter subscribe</h1>
                                <p>Nullam sit amet luctus ante, non faucibus massa. In euismod aliquam interdum. Maecenas varius aliquam arcu et convallis. Vivamus luctus in erat quis elementum. Vestibulum eleifend, lacus a blandit tincidunt, nunc libero facilisis nisl, vitae dignissim eros lectus eu neque.</p>
                                <form>
                                    <div class="row">
                                        <div class="large-12 columns">
                                            <div class="row collapse">
                                                <div class="small-9 columns">
                                                    <input type="text" placeholder="info@domain.me">
                                                </div>
                                                <div class="small-3 columns">
                                                    <a href="#" class="button postfix"><i class="fa fa-paper-plane"></i> <span class="hidden-for-small-only">Subscribe</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div><!-- .columns -->
                        </div><!-- .row -->
                    </div><!-- .inner -->
                </section><!-- .parallax-section -->



                <!-- ***** Partners section ***** -->
                <section class="partners-section">
                    <div class="inner">
                        <div class="row">
                            <div class="medium-12 columns">
                                <div class="ws-title left-line">
                                    <h1 class="title">My partners</h1>
                                </div>
                                <div class="carousel-wrapper">
                                    <div class="owl-carousel logo-carousel">
                                        <div class="item">
                                            <img alt="" src="assets/img/content/placeholder-low.gif">
                                        </div><!-- .item -->
                                        <div class="item">
                                            <img alt="" src="assets/img/content/placeholder-low.gif">
                                        </div><!-- .item -->
                                        <div class="item">
                                            <img alt="" src="assets/img/content/placeholder-low.gif">
                                        </div><!-- .item -->
                                        <div class="item">
                                            <img alt="" src="assets/img/content/placeholder-low.gif">
                                        </div><!-- .item -->
                                        <div class="item">
                                            <img alt="" src="assets/img/content/placeholder-low.gif">
                                        </div><!-- .item -->
                                        <div class="item">
                                            <img alt="" src="assets/img/content/placeholder-low.gif">
                                        </div><!-- .item -->
                                        <div class="item">
                                            <img alt="" src="assets/img/content/placeholder-low.gif">
                                        </div><!-- .item -->
                                        <div class="item">
                                            <img alt="" src="assets/img/content/placeholder-low.gif">
                                        </div><!-- .item -->
                                    </div><!-- .owl-carousel -->
                                </div><!-- .carousel-wrapper -->
                            </div><!-- .medium-12 -->
                        </div><!-- .row -->
                    </div><!-- .inner -->
                </section><!-- .partners -->



      @include('footer') 


                <!-- ***** Search modals ***** -->
                <div class="ps-modal-overlay" id="searchModal">
                    <a class="overlay-close"></a>
                    <div class="search row">
                        <div class="large-12 columns">
                            <form>
                                <div class="row collapse">
                                    <div class="small-8 columns">
                                        <input type="text" placeholder="Search...">
                                    </div>
                                    <div class="small-4 columns">
                                        <a href="#" class="button"><i class="fa fa-search"></i></a>
                                    </div>
                                </div><!-- /.row -->
                            </form>
                        </div><!-- /.large-12 -->
                    </div><!-- /.search -->
                    <div class="recommends row">
                        <div class="large-12 columns">
                            <div class="row">
                                <div class="large-4 columns">
                                    <h4>People</h4>
                                    <ul class="items">
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-circle">Guy Wilkins</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-circle">Larry Reed</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-circle">Roxanne Holland</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-circle">Bridget Kennedy</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-circle">Sally Mendoza</a></li>
                                    </ul><!-- /.items -->
                                </div><!-- /.large-4 -->
                                <div class="large-4 columns">
                                    <h4>Sermons</h4>
                                    <ul class="items">
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Guy Wilkins</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Larry Reed</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Roxanne Holland</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Bridget Kennedy</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Sally Mendoza</a></li>
                                    </ul><!-- /.items -->
                                </div><!-- /.large-4 -->
                                <div class="large-4 columns">
                                    <h4>Recent</h4>
                                    <ul class="items">
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumbe">Guy Wilkins</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Larry Reed</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Roxanne Holland</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Bridget Kennedy</a></li>
                                        <li><a href="#"><img alt="" src="assets/img/content/placeholder-thumb.gif" class="img-thumb">Sally Mendoza</a></li>
                                    </ul><!-- /.items -->
                                </div><!-- /.large-4 -->
                            </div><!-- /.row -->
                        </div><!-- /.large-12 -->
                    </div><!-- /.recommends -->
                </div><!-- /#searchModal -->

                <a class="exit-off-canvas"></a>

            </div>
        </div>
@include('scripts')
    </body>
</html>