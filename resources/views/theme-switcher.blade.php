<!-- ***** Theme switcher ***** -->
        <div class="theme-switcher" id="themeSwitcher">
            <a href="#" class="toggle-button"><i class="fa fa-gear"></i></a>

            <div class="inner">
                <h3>Color skin:</h3>
                <ul class="colors">
                    <li><a href="#" data-color="blue" class="color-blue"></a></li>
                    <li><a href="#" data-color="green" class="color-green"></a></li>
                    <li><a href="#" data-color="orange" class="color-orange"></a></li>
                    <li><a href="#" data-color="purple" class="color-purple"></a></li>
                    <li class="active"><a href="#" data-color="red" class="color-red"></a></li>
                    <li><a href="#" data-color="yellow" class="color-yellow"></a></li>
                </ul>
                <h3>Fixed menu:</h3>
                <input type="radio" checked class="fixedmenu" name="fixedmenu" value="1" id="fixed-navbar"><label for="fixed-navbar">On</label>
                <input type="radio" class="fixedmenu" name="fixedmenu" value="0" id="no-fixed-navbar"><label for="no-fixed-navbar">Off</label>
                <h3>Boxed layout:</h3>
                <input type="radio" class="boxed-layout" name="boxed-layout" value="1" id="boxed-layout"><label for="boxed-layout">On</label>
                <input type="radio" checked class="boxed-layout" name="boxed-layout" value="0" id="no-boxed-layout"><label for="no-boxed-layout">Off</label>
            </div>
        </div><!-- /.theme switcher -->