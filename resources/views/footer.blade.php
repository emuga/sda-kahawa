         <!-- ***** Footer section ***** -->
                <footer class="footer-section" id="footerSection">
                    <div class="content">
                        <div class="inner">
                            <div class="row">
                                <div class="large-3 columns">
                                    <img alt="" src="assets/img/elements/cross.png">
                                </div><!-- /.columns -->
                                <div class="large-3 columns info-box">

                                    <h1>About Seventh Day Adventist Church Kahawa Sukari</h1>
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p>

                                    <h1>Our church on Twitter</h1>
                                    <ul class="twitter-roll">
                                        <li><i class="fa fa-twitter-square"></i> <a href="#">@sdakahawasukari</a> ...follow us on twitter</li>
                                    </ul>
                                    <h1>Our church on facebook</h1>
                                    <ul class="twitter-roll">
                                        <li><i class="fa fa-facebook-official"></i> <a href="https://www.facebook.com/Kahawa-Sukari-SDA-Church-1905213296414422">@sdakahawasukari</a> ...follow us on facebook</li>
                                    </ul>

                                </div><!-- /.columns -->
                                <div class="large-3 columns blogroll-box">
                                    <h1>Quick Links</h1>

                                    <ul class="list-arrow">
                                        <li><a href="#">Sermons</a></li>
                                        <li><a href="{{route('dept')}}" >Ministries</a></li>
                                        <li><a href="#">Events</a></li>
                                        <li><a href="#">Projects</a></li>
                                        <li><a href="#">Gallery</a></li>
                                        <li><a href="#">Contact Us</a></li>
                                    </ul>
                                </div><!-- /.columns -->
                                <div class="large-3 columns contact-box">
                                    <img alt="" src="">
                                    <ul class="contact">
                                        <li>P.O.Box xxxxx 00200 Nairobi</li>
                                        <li>+25472000000</li>
                                        <li>sdakahawa.co.ke</li>
                                    </ul>
                                    <div class="text-center">
                                        <a href="#" class="button btn-dark">Contact us</a>
                                    </div>
                                    <ul class="socials">
                                        <li><a href="#" class="icon"><i class="fa fa-facebook-official"></i></a></li>
                                        <li><a href="#" class="icon"><i class="fa fa-twitter-square"></i></a></li>
                                        <li><a href="#" class="icon"><i class="fa fa-youtube-square"></i></a></li>
                                        <li><a href="#" class="icon"><i class="fa fa-pinterest-square"></i></a></li>
                                        <li><a href="#" class="icon"><i class="fa fa-skype"></i></a></li>
                                    </ul>
                                </div><!-- /.columns -->
                            </div><!-- /.row -->
                        </div><!-- /.inner -->
                    </div><!-- /.content -->
                    <div class="copyright">
                        <div class="inner">
                            <div class="row">
                                <div class="medium-10 columns">
                                    Seventh Day Adventist Church Kahawa Sukari 
                                </div>
                                <div class="medium-2 columns">
                                    <a href="pageTop" class="scroll-to"><i class="fa fa-angle-up"></i>Page top</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fixed-wrapper">
                        <div class="row">
                            <div class="medium-2 large-2 large-push-4 columns">
                                <img alt="" src="assets/img/content/placeholder-thumb.gif">
                            </div>
                            <div class="medium-10 large-4 large-pull-2 columns">
                                <div class="thanx-box">
                                    <h1>Thank You</h1>
                                    <h2>for visiting</h2>
                                    <p><span class="strong">Contact me</span> accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer><!-- #footerSection -->
