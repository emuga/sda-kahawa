 <!-- ***** Home revolution slider ***** -->
                <section class="home-slider-section">
                    <div class="rev_slider_wrapper">
                        <div id="slider1" class="rev_slider"  data-version="5.0">
                            <ul>

                                <li data-transition="fade">
                                    <img src="assets/img/content/worship.jpg"
                                         alt=""
                                         width="1920"
                                         height="1280"
                                         data-kenburns="on"
                                         data-bgposition="center center"
                                         data-duration="30000"
                                         data-ease="Power3.easeInOut"
                                         data-scalestart="100"
                                         data-scaleend="120"
                                         data-rotatestart="0"
                                         data-rotateend="0"
                                         data-offsetstart="0 100"
                                         data-offsetend="0 150"
                                         data-bgparallax="2"
                                         data-no-retina>

                                    <!--<div class="tp-caption News-Title"
                                         data-x="left" data-hoffset="800"
                                         data-y="top" data-voffset="200"
                                         data-whitespace="normal"
                                         data-transform_idle="o:1;"
                                         data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeInOut;"
                                         data-transform_out="x:50px;opacity:0;s:1000;e:Power2.easeInOut;"
                                         data-start="500">
                                        <div class="title">
                                            <h4>The</h4>
                                            <h1 style="color:#fff;">Birth</h1>
                                            <h3 style="color:#fff;">of Samuel</h3>
                                        </div>
                                    </div> -->

                                    <div class="tp-caption"
                                         data-x="left" data-hoffset="450"
                                         data-y="top" data-voffset="250"
                                         data-whitespace="normal"
                                         data-transform_idle="o:1;"
                                         data-transform_in="x:50px;opacity:0;s:1000;e:Power2.easeInOut;"
                                         data-transform_out="x:50px;opacity:0;s:1000;e:Power2.easeInOut;"
                                         data-start="500">
                                        <div class="quote" style="color:#fff;">
                                            And she made a vow, saying, Lord Almighty, if you will only look on your servant’s misery and remember me, and not forget your servant but give her a son, then I will give him to the Lord for all the days of his life, and no razor will ever be used on his head.<div class="small">1 Samuel 1:11</div>
                                        </div>
                                    </div>
                                </li>

                                <li data-transition="fade">
                                    <img src="assets/img/content/church-building.jpg"
                                         alt=""
                                         width="1920"
                                         height="1280"
                                         data-kenburns="on"
                                         data-bgposition="center center"
                                         data-duration="30000"
                                         data-ease="Power3.easeInOut"
                                         data-scalestart="100"
                                         data-scaleend="120"
                                         data-rotatestart="0"
                                         data-rotateend="0"
                                         data-offsetstart="250 100"
                                         data-offsetend="250 150"
                                         data-bgparallax="2"
                                         data-no-retina>
                                    <!--<div class="tp-caption News-Title"
                                         data-x="left" data-hoffset="400"
                                         data-y="top" data-voffset="200"
                                         data-whitespace="normal"
                                         data-transform_idle="o:1;"
                                         data-transform_in="x:-50px;opacity:0;s:2000;e:Power2.easeInOut;"
                                         data-transform_out="x:50px;opacity:0;s:2000;e:Power2.easeInOut;"
                                         data-start="500">
                                        <div class="title">
                                            <h4>The</h4>
                                            <h1>Birth</h1>
                                            <h3>of Samuel</h3>
                                        </div>
                                    </div>-->
                                    <div class="tp-caption"
                                         data-x="left" data-hoffset="450"
                                         data-y="top" data-voffset="250"
                                         data-whitespace="normal"
                                         data-transform_idle="o:1;"
                                         data-transform_in="x:50px;opacity:0;s:2000;e:Power2.easeInOut;"
                                         data-transform_out="x:50px;opacity:0;s:2000;e:Power2.easeInOut;"
                                         data-start="500">
                                        <div class="quote" style="color: white">
                                            And she made a vow, saying, Lord Almighty, if you will only look on your servant’s misery and remember me, and not forget your servant but give her a son, then I will give him to the Lord for all the days of his life, and no razor will ever be used on his head.<div class="small">1 Samuel 1:11</div>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div><!-- END REVOLUTION SLIDER -->
                    </div><!-- END OF SLIDER WRAPPER -->
                </section>



