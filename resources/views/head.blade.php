<head>

        <meta charset="utf-8">
        <title>Kahawa Sukari Seventh Day Adventist Church</title>

        <meta name="description" content="Kahawa Sukari Seventh Day Adventist Church">
        <meta name="keywords" content="God, Kahawa Sukari Seventh Day Adventist Church Nairobi">
        <meta name="author" content="EMuga">

        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Plugins styles -->
        <link rel="stylesheet" href="assets/css/plugins.css">

        <!-- Revolution slider -->
        <link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
        <link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
        <link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">

        <!-- Template styles -->
        <link rel="stylesheet" href="assets/css/backgrounds.css">
        <link rel="stylesheet" href="assets/css/style-red.css">

        <!-- Favicons -->
        <link rel="shortcut icon" href="assets/img/ico/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="assets/img/ico/apple-touch-icon.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="assets/img/ico/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="assets/img/ico/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/ico/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="assets/img/ico/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="assets/img/ico/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="assets/img/ico/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="assets/img/ico/apple-touch-icon-152x152.png" />

        
    </head>