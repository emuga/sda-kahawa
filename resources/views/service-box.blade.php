 <!-- ***** Service boxes section ***** -->
                <section class="services-boxes">
                    <div class="inner">
                        <div class="row">
                            <ul class="small-block-grid-1 medium-block-grid-3 services-blocks">
                                <li>
                                    <div class="inner">
                                        <div class="circle">
                                            <i class="fa fa-leaf"></i>
                                        </div>
                                        <div class="heading">
                                            <h6>Donate</h6>
                                            <p>Lorem ipsum dolor sit amet consect dipiscing elit sed diam nonummy.</p>
                                            <a href="#" class="button">Take a tour</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner">
                                        <div class="circle">
                                            <i class="fa fa-heart"></i>
                                        </div>
                                        <div class="heading">
                                            <h6>Service</h6>
                                            <p>Lorem ipsum dolor sit amet consect dipiscing elit sed diam nonummy.</p>
                                            <a href="#" class="button">Take a tour</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner">
                                        <div class="circle">
                                            <i class="fa fa-gear"></i>
                                        </div>
                                        <div class="heading">
                                            <h6>Help</h6>
                                            <p>Lorem ipsum dolor sit amet consect dipiscing elit sed diam nonummy.</p>
                                            <a href="#" class="button">Take a tour</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>