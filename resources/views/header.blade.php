<!-- ***** Header ***** -->
                <header class="header" id="header">
                    <div class="navigation" id="navigation">
                        <div class="contain-to-grid">
                            <div class="top-bar-small clearfix" id="topBarSmallMenu">
                                <div class="row">
                                    <div class="medium-12 columns">
                                        <ul class="left">
                                            <li><a href="#"><i class="flaticon-telephone46"></i> <span class="hide-for-small">+254720000000</span></a></li>
                                            <li><a href="#"><i class="flaticon-mail59"></i> <span class="hide-for-small">info@sdakahawa.co.ke</span></a></li>
                                        </ul>
                                        <ul class="right">
                                            <li><a href="https://www.facebook.com/Kahawa-Sukari-SDA-Church-1905213296414422" target="_blank">
                                                <i class="fa fa-facebook"></i> 
                                                <span class="hide-for-small">facebook</span></a>
                                            </li>
                                            <li><a href="#" class="">
                                                <i class="fa fa-youtube"></i> 
                                                <span class="hide-for-small">youtube</span></a>
                                            </li>
                                            <li><a href="#" class="left-off-canvas-toggle">
                                                <i class="flaticon-favourites7"></i> 
                                                <span class="hide-for-small">Favourites</span></a>
                                            </li>

<!--    <li><a href="shop-cart.html"><i class="flaticon-shopping232"></i><span class="counter">2</span>item</a></li>-->
                                            <li><a href="page-login.html" class="right-off-canvas-toggle"><i class="flaticon-open3"></i> <span class="hide-for-small">Login</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- /.topBarSmallMenu -->
                            <nav class="top-bar" id="topBar" data-topbar role="navigation" data-options="custom_back_text: true;back_text: Back;is_hover: true;mobile_show_parent_link: false; scrolltop : true">
                                <ul class="title-area">
                                    <li class="name">
                                        <h1><a href="#"><img alt="" height=100px width=100px src="assets/img/logo/logo.jpg"></a></h1>
                                    </li>
                                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                                </ul>
                                <section class="top-bar-section">
                                    <div class="top-bar-menu clearfix" id="topBarMenu">
                                        <ul class="left">
                                            <li class="active has-dropdown megamenu">
                                                <a href="#">Our Church</a>
                                                <ul class="dropdown">
                                                    <li>
                                                        <div class="mega-content row">
                                                            <div class="inner">
                                                                <div class="clearfix">
                                                                    
                                                                    <div class="medium-4 columns">
                                                                        <ul>
                                                                            <li><a href="#">About</a></li>
                                                                            <li><a href="{{route('dept')}}">Ministries/Departments</a></li>
                                                                            <li><a href="#">Sermons</a></li>
                                                                            <li><a href="#">Events</a></li>
                                                                            <li><a href="#">Gallery</a></li>
                                                                            <li><a href="#">Events</a></li>
                                                                            <li><a href="#">Blog</a></li>
                                                                        </ul>
                                                                    </div>
                                                                                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="has-dropdown">
                                                <a href="#">About</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Title One</a></li>
                                                    <li><a href="#l">Title Two</a></li>
                                                    <li><a href="#">Title Three</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="{{route('dept')}}">Ministries/Departments</a></li>
                                            
                                            <li class="has-dropdown">
                                                <a href="#">Sermons</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Topic 1</a></li>
                                                    <li><a href="#">Topic Two</a></li>
                                                    <li><a href="#">Topic Three</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">News/Events</a></li>
                                            <li><a href="#">Contact Us</a></li>
                                            <li class="mobile-search">
                                                <div class="row">
                                                    <div class="small-12 columns">
                                                        <form>
                                                            <div class="row collapse">
                                                                <div class="small-10 columns">
                                                                    <input type="text" placeholder="Search...">
                                                                </div>
                                                                <div class="small-2 columns">
                                                                    <a href="#" class="button expand">
                                                                        <i class="fa fa-search"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="desktop-search">
                                                <a class="btn-ps-modal" data-ps-modal-id="#searchModal">
                                                    <i class="fa fa-search"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div><!-- /.topBarMenu -->
                                </section><!-- /.top-bar-section -->
                            </nav><!-- /nav -->
                        </div><!-- /.fixed -->
                    </div><!-- /#navigation -->
                </header><!-- /#header -->