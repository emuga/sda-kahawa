<!-- ***** Right off canvas ***** -->
                <aside class="right-off-canvas-menu">
                    <form>
                        <div class="row login-offcanvas">
                            <div class="large-12 columns login-pic">
                                <img alt="" src="assets/img/content/placeholder-thumb.gif">
                            </div>
                            <div class="large-12 columns login-text">
                                Nam felis diam, finibus sit amet arcu eu, fringilla posuere urna. Maecenas at ornare nibh. Pellentesque id maximus nisl, sit amet sagittis massa.
                            </div>
                            <div class="large-12 columns">
                                <div class="row collapse">
                                    <div class="small-9 columns">
                                        <input type="text" placeholder="E-mail">
                                    </div>
                                    <div class="small-3 columns">
                                        <span class="postfix"><i class="fa fa-user"></i></span>
                                    </div>
                                </div>
                                <div class="row collapse">
                                    <div class="small-9 columns">
                                        <input type="password" placeholder="Password">
                                    </div>
                                    <div class="small-3 columns">
                                        <span class="postfix"><i class="fa fa-lock"></i></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-12 columns">
                                        <input id="checkbox1" class="checkbox" type="checkbox"><label for="checkbox1">Remember me</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-12 columns">
                                        <a href="#" class="small">Forgot password?</a>
                                    </div>
                                </div>
                                <button type="submit" class="button">Login</button>
                            </div>
                        </div>
                    </form>
                </aside>