  <!-- ***** Left off canvas ***** -->
                <aside class="left-off-canvas-menu">
                    <div class="row">
                        <div class="favourites offcanvas">
                            <div class="item">
                                <div class="row">
                                    <div class="small-4 columns">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                    </div>
                                    <div class="small-8 columns">
                                        <h1>Nam felis diam, finibus sit amet arcu eu lorem ipsu</h1>
                                        <ul class="stars">
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                        </ul>
                                        <a href="blog-single.html" class="btn-more">More</a>
                                        <a href="#" class="btn-remove">Remove</a>
                                    </div>
                                </div>
                            </div><!-- /.item -->
                            <div class="item">
                                <div class="row">
                                    <div class="small-4 columns">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                    </div>
                                    <div class="small-8 columns">
                                        <h1>Nam felis diam, finibus sit amet arcu eu lorem ipsu</h1>
                                        <ul class="stars">
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                        </ul>
                                        <a href="blog-single.html" class="btn-more">More</a>
                                        <a href="#" class="btn-remove">Remove</a>
                                    </div>
                                </div>
                            </div><!-- /.item -->
                            <div class="item">
                                <div class="row">
                                    <div class="small-4 columns">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                    </div>
                                    <div class="small-8 columns">
                                        <h1>Nam felis diam, finibus sit amet arcu eu lorem ipsu</h1>
                                        <ul class="stars">
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                        </ul>
                                        <a href="blog-single.html" class="btn-more">More</a>
                                        <a href="#" class="btn-remove">Remove</a>
                                    </div>
                                </div>
                            </div><!-- /.item -->
                            <div class="item">
                                <div class="row">
                                    <div class="small-4 columns">
                                        <img alt="" src="assets/img/content/placeholder-thumb.gif">
                                    </div>
                                    <div class="small-8 columns">
                                        <h1>Nam felis diam, finibus sit amet arcu eu lorem ipsu</h1>
                                        <ul class="stars">
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li class="active"><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#" class="icon"><i class="fa fa-star"></i></a></li>
                                        </ul>
                                        <a href="blog-single.html" class="btn-more">More</a>
                                        <a href="#" class="btn-remove">Remove</a>
                                    </div>
                                </div>
                            </div><!-- /.item -->
                        </div>
                    </div>
                </aside><!-- /.left-off-canvas-menu -->

